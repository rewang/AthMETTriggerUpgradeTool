#!/usr/bin/env bash


#rucio list-dids "user.rewang.mc15_13TeV.159000.PG_nu_E50.s2576.trkmhtonline.01-00_myOutput.root"
#rucio download --ndownloader 5 "user.rewang.mc15_13TeV.159000.PG_nu_E50.s2576.trkmhtonline.01-00_myOutput.root" --dir /tmp/rewang/

#rucio list-dids "mc15_13TeV.159000.ParticleGenerator_nu_E50.merge.AOD.e3711_s2576_s2132_r8902_r8944"
#rucio download --nrandom 2 --ndownloader 5 "mc15_13TeV.159000.ParticleGenerator_nu_E50.merge.AOD.e3711_s2576_s2132_r8902_r8944" --dir /tmp/rewang/


#rucio list-dids "mc15_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.merge.AOD*"

#ami list datasets mc15_13TeV.159000.ParticleGenerator_nu_E50.merge.AOD.%
#ami list datasets mc15_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.merge.AOD.%
#ami list datasets mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.%

#rucio download --nrandom 1 --ndownloader 5 "mc15_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.merge.AOD.e3601_s2576_s2132_r8902_r8944" --dir /tmp/rewang/ &
#rucio download --nrandom 1 --ndownloader 5 "mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e3601_s2576_s2132_r8902_r8944" --dir /tmp/rewang/ &

#rucio download --nrandom 25 --ndownloader 5 "valid1.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.recon.AOD.e5112_s3091_r9123" --dir /tmp/rewang/ &
#rucio download --nrandom 5 --ndownloader 5 "valid1.361405.Sherpa_CT10_Zmumu_Pt280_500_CVetoBVeto.recon.AOD.e5112_s3091_r9123" --dir /tmp/rewang/ &
#rucio download --nrandom 5 --ndownloader 5 "valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e4993_s3091_r9123" --dir /tmp/rewang/ &
#rucio download --nrandom 5 --ndownloader 5 "valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e4993_s2887_r9043" --dir /tmp/rewang/ &




##############
## MC16
##############

#rucio list-dids "mc16_13TeV.429007.PowhegPythia_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.*"
#rucio download --nrandom 5 "mc16_13TeV.429007.PowhegPythia_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3714_s3126_r9546_r9551" --dir /tmp/rewang/ &


#rucio list-dids "mc16_13TeV.301118.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu_5000M.merge.AOD.*"
#rucio download --nrandom 1 --ndownloader 5 "mc16_13TeV.301118.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu_5000M.merge.AOD.e3663_s3126_r9546_r9551"  --dir /tmp/rewang/ &


#rucio list-dids "mc16_13TeV.301078.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu_5000M.merge.AOD.*"
#rucio download --nrandom 5 --ndownloader 5 "mc16_13TeV.301078.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu_5000M.merge.AOD.e3663_s3126_r9546_r9551"  --dir /tmp/rewang/ &
