###########
import AthenaCommon.AtlasUnixStandardJob
from AthenaCommon.AlgSequence import AlgSequence
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
from AthenaCommon.AppMgr import theApp
from AthMETTriggerUpgradeTool.AthMETTriggerUpgradeToolConf import AthMETTriggerUpgradeTool
import AthenaRootComps.ReadAthenaxAOD
topSequence = AlgSequence()
theApp.EvtMax=-1
#svcMgr.EventSelector.InputCollections=[' /tmp/rewang/DAOD_TRUTH1.NAME.root ']
AthMETTriggerUpgradeTool  = AthMETTriggerUpgradeTool( 'AthMETTriggerUpgradeTool ')
#AthMETTriggerUpgradeTool.InputFile = ' /tmp/rewang/DAOD_TRUTH1.NAME.root '
topSequence += AthMETTriggerUpgradeTool
svcMgr.EventSelector.AccessMode=2
svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["ANOTHERSTREAM DATAFILE=' DAOD_TRUTH1.NAME.TREE.root ' OPT='RECREATE'"]
