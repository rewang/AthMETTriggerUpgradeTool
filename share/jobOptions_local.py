###########
import AthenaCommon.AtlasUnixStandardJob
from AthenaCommon.AlgSequence import AlgSequence
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
from AthenaCommon.AppMgr import theApp
from AthMETTriggerUpgradeTool.AthMETTriggerUpgradeToolConf import AthMETTriggerUpgradeTool
import AthenaRootComps.ReadAthenaxAOD
topSequence = AlgSequence()
theApp.EvtMax=-1
#svcMgr.EventSelector.InputCollections=[' /tmp/rewang/DAOD_TRUTH1.NAME.root ']
AthMETTriggerUpgradeTool  = AthMETTriggerUpgradeTool( 'AthMETTriggerUpgradeTool ')
#AthMETTriggerUpgradeTool.InputFile = ' /tmp/rewang/DAOD_TRUTH1.NAME.root '
topSequence += AthMETTriggerUpgradeTool
svcMgr.EventSelector.AccessMode=2
svcMgr += CfgMgr.THistSvc()
#svcMgr.THistSvc.Output += ["ANOTHERSTREAM DATAFILE=' DAOD_TRUTH1.NAME.TREE.root ' OPT='RECREATE'"]

#svcMgr.THistSvc.Output += ["ANOTHERSTREAM DATAFILE=' mc16_13TeV.429007.PowhegPythia_P2012_ttbar_hdamp172p5_nonallhad.root ' OPT='RECREATE'"]
#svcMgr.EventSelector.InputCollections=[' /tmp/rewang/mc16_13TeV.429007.PowhegPythia_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3714_s3126_r9546_r9551/AOD.11372672._000001.pool.root.1 ']
#AthMETTriggerUpgradeTool.InputFile = ' /tmp/rewang/mc16_13TeV.429007.PowhegPythia_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3714_s3126_r9546_r9551/AOD.11372672._000001.pool.root.1 '



#svcMgr.THistSvc.Output += ["ANOTHERSTREAM DATAFILE=' mc16_13TeV.301078.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu_5000M.root ' OPT='RECREATE'"]
#svcMgr.EventSelector.InputCollections=['/tmp/rewang/mc16_13TeV.301078.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu_5000M.merge.AOD.e3663_s3126_r9546_r9551/AOD.11372405._000001.pool.root.1 ']
#AthMETTriggerUpgradeTool.InputFile = ' /tmp/rewang/mc16_13TeV.301078.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu_5000M.merge.AOD.e3663_s3126_r9546_r9551/AOD.11372405._000001.pool.root.1 '

svcMgr.THistSvc.Output += ["ANOTHERSTREAM DATAFILE=' data17_13TeV.00325713.physics_ZeroBias.root ' OPT='RECREATE'"]
svcMgr.EventSelector.InputCollections=[' /eos/atlas/atlastier0/rucio/data17_13TeV/physics_ZeroBias/00325713/data17_13TeV.00325713.physics_ZeroBias.merge.AOD.x514_m1807/data17_13TeV.00325713.physics_ZeroBias.merge.AOD.x514_m1807._lb0090._0001.1 ']


#AthMETTriggerUpgradeTool.InputFile = ' /eos/atlas/atlastier0/rucio/data17_13TeV/physics_ZeroBias/00325713/data17_13TeV.00325713.physics_ZeroBias.merge.AOD.x514_m1807'


