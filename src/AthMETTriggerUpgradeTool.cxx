// AthMETTriggerUpgradeTool includes
#include "AthMETTriggerUpgradeTool.h"


// STD include(s):
#include <algorithm>    // std::sort
#include <stdio.h>      // sscanf

#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"

#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"

#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETComposition.h"

#include "PathResolver/PathResolver.h"

AthMETTriggerUpgradeTool::AthMETTriggerUpgradeTool( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator )
{

    //declareProperty( "Property", m_nProperty ); //example property declaration
    declareProperty("InputFile",m_sampleName="","InputContainer");
    declareProperty("TrigDecisionTool", m_trigDecTool);

}


AthMETTriggerUpgradeTool::~AthMETTriggerUpgradeTool() {}



StatusCode AthMETTriggerUpgradeTool::initialize()
{
    ATH_MSG_INFO ("Initializing " << name() << "...");

    // count number of events
    m_eventCounter = 0;

/*
    // Initialize and configure trigger tools
    m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    CHECK( m_trigConfigTool->initialize() );
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
    m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    CHECK(m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
    CHECK(m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
    CHECK(m_trigDecisionTool->initialize() );
*/

//    // Create the tool(s) to test:
//    objTool = new ST::SUSYObjDef_xAOD("SUSYObjDef_xAOD");
//    std::cout << " ABOUT TO INITIALIZE SUSYTOOLS " << std::endl;
//    ///////////////////////////////////////////////////////////////////////////////////////////
//    std::string config_file = "SUSYTools/SUSYTools_Default.conf";
//    std::string prw_file = "DUMMY";
//    std::string ilumicalc_file = "DUMMY";
//
//    bool isData = false;
//    bool isAtlfast = false;
//    ST::ISUSYObjDef_xAODTool::DataSource datasource = (isData ? ST::ISUSYObjDef_xAODTool::Data : (isAtlfast ? ST::ISUSYObjDef_xAODTool::AtlfastII : ST::ISUSYObjDef_xAODTool::FullSim));
//
//
//    // Configure the SUSYObjDef instance
//    ANA_CHECK( objTool->setProperty("DataSource", datasource) ) ;
//    if(!config_file.empty())
//        ANA_CHECK( objTool->setProperty("ConfigFile", config_file) );
//
//    //ANA_CHECK( objTool->setBoolProperty("METDoTrkSyst", true) );
//    //ANA_CHECK( objTool->setBoolProperty("METDoCaloSyst", false) );
//
//    std::vector<std::string> prw_conf;
//    if (prw_file == "DUMMY") {
//        //prw_conf.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15ab_defaults.NotRecommended.prw.root");
//        prw_conf.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root");
//    }
//
//    ANA_CHECK( objTool->setProperty("PRWConfigFiles", prw_conf) );
//
//
//    std::vector<std::string> prw_lumicalc;
//    if (ilumicalc_file == "DUMMY") {
//        prw_lumicalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root"));
//        prw_lumicalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data16_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root"));
//    }
//
//    ANA_CHECK( objTool->setProperty("PRWLumiCalcFiles", prw_lumicalc) );
//
//    objTool->initialize();
//    std::cout << " INITIALIZED SUSYTOOLS " << std::endl;






    tree_=new TTree("data","data");

    ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
    CHECK(histSvc.retrieve());

    histSvc->regTree("/ANOTHERSTREAM/Tree",tree_);

    tree_->Branch("event_num", &event_num);
    tree_->Branch("run_num", &run_num);
    tree_->Branch("npv", &npv);
    tree_->Branch("actMU", &actMU);
    tree_->Branch("avgMU", &avgMU);

    tree_->Branch("HLT_xe110_pufit_L1XE60", &HLT_xe110_pufit_L1XE60);
    tree_->Branch("HLT_xe120_pufit_L1XE60", &HLT_xe120_pufit_L1XE60);
    tree_->Branch("HLT_xe110_pufit_wEFMu_L1XE60", &HLT_xe110_pufit_wEFMu_L1XE60);
    tree_->Branch("HLT_xe120_mht_xe80_L1XE60", &HLT_xe120_mht_xe80_L1XE60);
    tree_->Branch("HLT_xe120_trkmht_xe80_mht_L1XE60", &HLT_xe120_trkmht_xe80_mht_L1XE60);
    tree_->Branch("L1_KF_XE60", &L1_KF_XE60);

    tree_->Branch("passL1XE50", &passL1XE50);
    tree_->Branch("L1XE", &L1XE);

    tree_->Branch("HLT_mht_metx", &HLT_mht_metx);
    tree_->Branch("HLT_mht_mety", &HLT_mht_mety);
    tree_->Branch("HLT_cell_metx", &HLT_cell_metx);
    tree_->Branch("HLT_cell_mety", &HLT_cell_mety);
    tree_->Branch("HLT_topocl_metx", &HLT_topocl_metx);
    tree_->Branch("HLT_topocl_mety", &HLT_topocl_mety);
    tree_->Branch("HLT_pufit_metx", &HLT_pufit_metx);
    tree_->Branch("HLT_pufit_mety", &HLT_pufit_mety);
    tree_->Branch("HLT_pueta_metx", &HLT_pueta_metx);
    tree_->Branch("HLT_pueta_mety", &HLT_pueta_mety);
    tree_->Branch("HLT_trkmht_metx", &HLT_trkmht_metx);
    tree_->Branch("HLT_trkmht_mety", &HLT_trkmht_mety);





    tree_->Branch("genmet_px", &genmet_px);
    tree_->Branch("genmet_py", &genmet_py);


    tree_->Branch("raw_met_ele", &raw_met_ele);
    tree_->Branch("raw_met_gamma", &raw_met_gamma);
    tree_->Branch("raw_met_tau", &raw_met_tau);
    tree_->Branch("raw_met_jet", &raw_met_jet);
    tree_->Branch("raw_met_muons", &raw_met_muons);
    tree_->Branch("raw_met_softtrk", &raw_met_softtrk);
    tree_->Branch("raw_met", &raw_met);

    tree_->Branch("reco_met_ele", &reco_met_ele);
    tree_->Branch("reco_met_gamma", &reco_met_gamma);
    tree_->Branch("reco_met_tau", &reco_met_tau);
    tree_->Branch("reco_met_jet", &reco_met_jet);
    tree_->Branch("reco_met_muons", &reco_met_muons);
    tree_->Branch("reco_met_softtrk", &reco_met_softtrk);
    tree_->Branch("reco_met", &reco_met);
    tree_->Branch("reco_metphi", &reco_metphi);


    return StatusCode::SUCCESS;
}

StatusCode AthMETTriggerUpgradeTool::finalize()
{
    ATH_MSG_INFO ("Finalizing " << name() << "...");

//    if( objTool ) {
//        delete objTool;
//        objTool = 0;
//    }

//
//    if( m_trigDecisionTool ) {
//        delete m_trigDecisionTool;
//        m_trigDecisionTool = 0;
//    }


    return StatusCode::SUCCESS;
}

StatusCode AthMETTriggerUpgradeTool::execute()
{

    using namespace std;
    ATH_MSG_DEBUG ("Executing " << name() << "...");



    // print every 1000 events, so we know where we are:
    if( (m_eventCounter % 1000) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
    m_eventCounter++;


    //objTool->ApplyPRWTool();



    HLT_xe110_pufit_L1XE60 = m_trigDecTool->isPassed("HLT_xe110_pufit_L1XE60");
    HLT_xe120_pufit_L1XE60 = m_trigDecTool->isPassed("HLT_xe120_pufit_L1XE60");
    HLT_xe110_pufit_wEFMu_L1XE60 = m_trigDecTool->isPassed("HLT_xe110_pufit_wEFMu_L1XE60");
    HLT_xe120_mht_xe80_L1XE60 = m_trigDecTool->isPassed("HLT_xe120_mht_xe80_L1XE60");
    HLT_xe120_trkmht_xe80_mht_L1XE60 = m_trigDecTool->isPassed("HLT_xe120_trkmht_xe80_mht_L1XE60");
    L1_KF_XE60 = m_trigDecTool->isPassed("L1_KF-XE60");

    /////////////////////////////////////////////////////////////////
    //
    // Event information
    //
    /////////////////////////////////////////////////////////////////
    const xAOD::EventInfo* eventInfo = 0; //NOTE: Everything that comes from the storegate direct from the input files is const!

    // ask the event store to retrieve the xAOD EventInfo container
    //CHECK( evtStore()->retrieve( eventInfo, "EventInfo") );  // the second argument ("EventInfo") is the key name
    CHECK( evtStore()->retrieve( eventInfo) );
    // if there is only one container of that type in the xAOD (as with the EventInfo container), you do not need to pass
    // the key name, the default will be taken as the only key name in the xAOD

    // check if data or MC
    bool isMC = true;
    if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION ) ){
        isMC = false;
    }


//    // if data check if event passes GRL
//    if(!isMC) { // it's data!
//        if(!m_grl->passRunLB(*eventInfo)) {
//            Info("execute()", "event passing GRL failed !!!");
//            return EL::StatusCode::SUCCESS; // go to next event
//        }
//    } // end if not MC


    //------------------------------------------------------------
    // Apply event cleaning to remove events due to
    // problematic regions of the detector
    // or incomplete events.
    // Apply to data.
    //------------------------------------------------------------
    // reject event if:
    if(!isMC) {
        if(   (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error )
              || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error )
              || (eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error )
              || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) ) {
            Info("execute()", "problematic regions of the detector, event cleaning !!!");
            return StatusCode::SUCCESS; // go to the next event
        } // end if event flags check
    } // end if the event is data


    event_num = eventInfo->eventNumber();
    run_num = eventInfo->runNumber();

    if (isMC) {
        run_num = eventInfo->mcChannelNumber();
    }

    actMU = eventInfo->actualInteractionsPerCrossing();
    avgMU = eventInfo->averageInteractionsPerCrossing();

    /////////////////////////////////////////////////////////////////
    //
    // Reconstructed vertex
    //
    /////////////////////////////////////////////////////////////////
    const xAOD::VertexContainer* vertices = 0;
    CHECK( evtStore()->retrieve( vertices, "PrimaryVertices") );
    bool hasOfflineGoodVtx = false;
    for (auto vertex: *vertices) {
        //count the number of vertices with at least 2 tracks
        if ( vertex->nTrackParticles() < 2 ) continue;
        if (vertex->vertexType() == xAOD::VxType::PriVtx) {
            hasOfflineGoodVtx = true;
            break;
        }
    }
    if(!hasOfflineGoodVtx) return StatusCode::SUCCESS;
    npv = vertices->size();


    /////////////////////////////////////////////////////////////////
    //
    // L1 and HL Trigger MET
    //
    /////////////////////////////////////////////////////////////////
    //get the L1 MET, make sure the L1MET threshold is calculated correctly
    const xAOD::EnergySumRoI* l1MetObject(nullptr);
    CHECK( evtStore()->retrieve(l1MetObject, "LVL1EnergySumRoI") );
    if(l1MetObject!=nullptr) {
        float l1_mex = l1MetObject->exMiss() * 0.001;
        float l1_mey = l1MetObject->eyMiss() * 0.001;
        float l1_met(0.);
        bool l1_overflow(false);
        calcL1MET(l1_mex, l1_mey, l1_met, l1_overflow);
        passL1XE50 = l1_overflow || l1_met > 50;
        //if(!passL1XE50) return EL::StatusCode::SUCCESS;
        L1XE = l1_met;
        //std::cout << "L1XE: " << L1XE << std::endl;
    }



    /////////////////////////////////////
    //
    // HLT MET
    //
    /////////////////////////////////////
    const xAOD::TrigMissingETContainer* hltCont_mht(nullptr);
    CHECK( evtStore()->retrieve( hltCont_mht, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht") );
    if(hltCont_mht!=nullptr) {
        HLT_mht_metx = hltCont_mht->front()->ex() * 0.001;
        HLT_mht_mety = hltCont_mht->front()->ey() * 0.001;
    }

//    std::cout << "HLT_mht_metx: " << HLT_mht_metx
//              << " HLT_mht_mety: " << HLT_mht_mety
//              << std::endl;


    const xAOD::TrigMissingETContainer* hltCont_cell(nullptr);
    CHECK( evtStore()->retrieve( hltCont_cell, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET") );
    if(hltCont_cell!=nullptr) {
        HLT_cell_metx = hltCont_cell->front()->ex() * 0.001;
        HLT_cell_mety = hltCont_cell->front()->ey() * 0.001;
    }


    const xAOD::TrigMissingETContainer* hltCont_topocl(nullptr);
    CHECK( evtStore()->retrieve( hltCont_topocl, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl") );
    if(hltCont_topocl!=nullptr) {
        HLT_topocl_metx = hltCont_topocl->front()->ex() * 0.001;
        HLT_topocl_mety = hltCont_topocl->front()->ey() * 0.001;
    }


    const xAOD::TrigMissingETContainer* hltCont_pufit(nullptr);
    CHECK( evtStore()->retrieve( hltCont_pufit, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PUC") );
    if(hltCont_pufit!=nullptr) {
        HLT_pufit_metx = hltCont_pufit->front()->ex() * 0.001;
        HLT_pufit_mety = hltCont_pufit->front()->ey() * 0.001;
    }


    //this does not exist any longer
//    const xAOD::TrigMissingETContainer* hltCont_pueta(nullptr);
//    CHECK( evtStore()->retrieve( hltCont_pueta, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PS") );
//    if(hltCont_pueta!=nullptr) {
//        HLT_pueta_metx = hltCont_pueta->front()->ex() * 0.001;
//        HLT_pueta_mety = hltCont_pueta->front()->ey() * 0.001;
//    }


    //cannot read trkmht ??
//    const xAOD::TrigMissingETContainer* hltCont_trkmht(nullptr);
//    CHECK( evtStore()->retrieve( hltCont_trkmht, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_trkmht") );
//    if(hltCont_trkmht!=nullptr) {
//        HLT_trkmht_metx = hltCont_trkmht->front()->ex() * 0.001;
//        HLT_trkmht_mety = hltCont_trkmht->front()->ey() * 0.001;
//    }


    /////////////////////////////////////////////////////////////////////////////////
    //
    // Truth information
    //
    /////////////////////////////////////////////////////////////////////////////////

    // met
    // Retrieve the truth met
    if(isMC) {
        const xAOD::MissingETContainer *missingET = 0;
        CHECK(evtStore()->retrieve(missingET, "MET_Truth"));
        genmet_px = (*missingET)["NonInt"]->mpx() * 0.001;
        genmet_py = (*missingET)["NonInt"]->mpy() * 0.001;
    }



    /////////////////////////////////////////////////////////////////////////////////
    //
    // Offline Parts
    //
    /////////////////////////////////////////////////////////////////////////////////

    //get offline MET
    const xAOD::MissingETContainer* metCont = 0;
    std::string refMetKey = "MET_Reference_" + jetType;
    refMetKey.erase(refMetKey.length() - 4);//this removes the Jets from the end of the jetType
    CHECK( evtStore()->retrieve( metCont, refMetKey) );
    //for(const auto& met : *metCont) {
    //  cout << met->name() << endl;
    //}


    const xAOD::MissingET* metRaw = (*metCont)["FinalTrk"];
    raw_met = metRaw->met() * 0.001;

    const xAOD::MissingET *metRaw_ele = (*metCont)["RefEle"];
    const xAOD::MissingET *metRaw_gamma = (*metCont)["RefGamma"];
    const xAOD::MissingET *metRaw_tau = (*metCont)["RefTau"];
    const xAOD::MissingET *metRaw_jet = (*metCont)["RefJet"];
    const xAOD::MissingET *metRaw_muons = (*metCont)["Muons"];
    const xAOD::MissingET *metRaw_softtrk = (*metCont)["PVSoftTrk"];

    raw_met_ele = metRaw_ele->met() * 0.001;
    raw_met_gamma = metRaw_gamma->met() * 0.001;
    raw_met_tau = metRaw_tau->met() * 0.001;
    raw_met_jet = metRaw_jet->met() * 0.001;
    raw_met_muons = metRaw_muons->met() * 0.001;
    raw_met_softtrk = metRaw_softtrk->met() * 0.001;




/*
    ////////////////////////////////////////////////////////////////
    //
    // Making MET (with calibration)
    //
    ////////////////////////////////////////////////////////////////
    //retrieve the original containers
    const xAOD::MissingETContainer* coreMet  = nullptr;
    std::string coreMetKey = "MET_Core_" + jetType;
    coreMetKey.erase(coreMetKey.length() - 4);//this removes the Jets from the end of the jetType
    CHECK( evtStore()->retrieve(coreMet, coreMetKey) );

    //retrieve the MET association map
    const xAOD::MissingETAssociationMap* metMap = nullptr;
    std::string metAssocKey = "METAssoc_" + jetType;
    metAssocKey.erase(metAssocKey.length() - 4 );//this removes the Jets from the end of the jetType
    CHECK( evtStore()->retrieve(metMap, metAssocKey) );

    // Create your output MET (aux) container
    xAOD::MissingETContainer*    newMetContainer    = new xAOD::MissingETContainer();
    xAOD::MissingETAuxContainer* newMetAuxContainer = new xAOD::MissingETAuxContainer();
    newMetContainer->setStore(newMetAuxContainer);

    metMap->resetObjSelectionFlags();



    //--------------
    //Electrons
    //-------------

    // Get the nominal object containers from the event
    // Electrons
    xAOD::ElectronContainer* electrons(0);
    xAOD::ShallowAuxContainer* electrons_aux(0);
    ANA_CHECK( objTool->GetElectrons(electrons, electrons_aux) );
//    const xAOD::ElectronContainer* electrons = nullptr;
//    assert( event->retrieve(electrons, "Electrons") );

    ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);

    for(const auto& el : *electrons) {

        float ele_pt = el->pt() * 0.001 ;
        float ele_eta = el->eta();
        //float ele_phi = el->phi();
        //float ele_en = el->e() * 0.001;

        if(ele_pt<10) continue;
        if(!(fabs(ele_eta)<1.37 || (fabs(ele_eta)>1.52 && fabs(ele_eta)<2.47)) ) continue;

        metElectrons.push_back(el);
    }


    assert(metMaker->rebuildMET("RefEle",                   //name of metElectrons in metContainer
                                xAOD::Type::Electron,       //telling the rebuilder that this is electron met
                                newMetContainer,            //filling this met container
                                metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
                                metMap)                     //and this association map
    );


    //--------------
    // Muons
    //-------------
    xAOD::MuonContainer* muons(0);
    xAOD::ShallowAuxContainer* muons_aux(0);
    ANA_CHECK( objTool->GetMuons(muons, muons_aux) );
//    const xAOD::PhotonContainer* photons = nullptr;
//    assert( event->retrieve(photons, "Photons"));

    ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
    for(const auto& mu : *muons) {

        float mu_pt = mu->pt() * 0.001;
        float mu_eta = mu->eta();
        //float mu_phi = mu->phi();
        //float mu_en = mu->e() * 0.001;

        if (mu_pt < 10 || fabs(mu_eta) > 2.5) continue;

        metMuons.push_back(mu);

    }

    assert(metMaker->rebuildMET("Muons",
                                xAOD::Type::Muon,
                                newMetContainer,
                                metMuons.asDataVector(),
                                metMap)
    );



    //--------------
    // Photons
    //--------------
    xAOD::PhotonContainer* photons(0);
    xAOD::ShallowAuxContainer* photons_aux(0);
    ANA_CHECK( objTool->GetPhotons(photons,photons_aux) );

    ConstDataVector<xAOD::PhotonContainer> metPhotons(SG::VIEW_ELEMENTS);
    for(const auto& ph : *photons) {

        float ph_pt = ph->pt() * 0.001 ;
        float ph_eta = ph->eta();
        //float ph_phi = ph->phi();
        //float ph_en = ph->e() * 0.001;

        if(ph_pt<25) continue;
        if(!(fabs(ph_eta)<1.37 || (fabs(ph_eta)>1.52 && fabs(ph_eta)<2.37) )) continue;


        metPhotons.push_back(ph);
    }

    assert(metMaker->rebuildMET("RefGamma",
                                xAOD::Type::Photon,
                                newMetContainer,
                                metPhotons.asDataVector(),
                                metMap)
    );






    //--------------
    // Taus
    //--------------

    xAOD::TauJetContainer* taus(0);
    xAOD::ShallowAuxContainer* taus_aux(0);
    ANA_CHECK( objTool->GetTaus(taus,taus_aux) );
//
//    const xAOD::TauJetContainer* taus = nullptr;
//    assert( event->retrieve(taus, "TauJets"));


    ConstDataVector<xAOD::TauJetContainer> metTaus(SG::VIEW_ELEMENTS);
    for(const auto& tau : *taus) {

        float tau_pt = tau->pt() * 0.001 ;
        float tau_eta = tau->eta();
        //float tau_taui = tau->taui();
        //float tau_en = tau->e() * 0.001;

        if(tau_pt<20) continue;
        if(!(fabs(tau_eta)<1.37 || (fabs(tau_eta)>1.52 && fabs(tau_eta)<2.37)) ) continue;
        if(tau->nTracks()==0) continue;


        metTaus.push_back(tau);
    }

    assert(metMaker->rebuildMET("RefTau",
                                xAOD::Type::Tau,
                                newMetContainer,
                                metTaus.asDataVector(),
                                metMap)
    );




    //--------------
    //Jets
    //--------------

//        const xAOD::JetContainer* jets = nullptr;
//        assert( event->retrieve(jets, jetType));

    xAOD::JetContainer* jets(0);
    xAOD::ShallowAuxContainer* jets_aux(0);
    ANA_CHECK( objTool->GetJets(jets, jets_aux) );



    //this line will mark the electron as invisible if it passes the (inv) electron selection cut
    //this removes the particle and associated clusters from the jet and soft term calculations
    assert( metMaker->markInvisible(metMuons.asDataVector(),metMap) );
    // NOTE: Objects marked as invisible should not also be added as part
    // of another term! However, you can e.g. mark some electrons invisible
    // and compute RefEle with others.

    // met::addGhostMuonsToJets(*uncalibMuons, *jets);



    //Now time to rebuild jetMet and get the soft term
    //This adds the necessary soft term for both CST and TST
    //these functions create an xAODMissingET object with the given names inside the container
    assert(  metMaker->rebuildJetMET("RefJet",        //name of jet met
                                     "SoftClus",      //name of soft cluster term met
                                     "PVSoftTrk",     //name of soft track term met
                                     newMetContainer, //adding to this new met container
                                     jets,       //using this jet collection to calculate jet met
                                     coreMet,         //core met container
                                     metMap,          //with this association map
                                     true            //apply jet jvt cut
             )
    );

    //this builds the final track or cluster met sums, using systematic varied container
    //In the future, you will be able to run both of these on the same container to easily output CST and TST
    assert( metMaker->buildMETSum("TST" , newMetContainer, MissingETBase::Source::Track ) );
    const xAOD::MissingET* newmet_tst = (*newMetContainer)["TST"];
    //for(const auto& met : *newMetContainer) {
    //    cout << met->name() << endl;
    //}

    reco_met = newmet_tst->met() * 0.001 ;
    reco_metphi = newmet_tst->phi();

    const xAOD::MissingET *newmet_tst_ele = (*newMetContainer)["RefEle"];
    const xAOD::MissingET *newmet_tst_gamma = (*newMetContainer)["RefGamma"];
    const xAOD::MissingET *newmet_tst_tau = (*newMetContainer)["RefTau"];
    const xAOD::MissingET *newmet_tst_jet = (*newMetContainer)["RefJet"];
    const xAOD::MissingET *newmet_tst_muons = (*newMetContainer)["Muons"];
    const xAOD::MissingET *newmet_tst_softtrk = (*newMetContainer)["PVSoftTrk"];

    reco_met_ele        = newmet_tst_ele->met() * 0.001;
    reco_met_gamma      = newmet_tst_gamma->met() * 0.001;
    reco_met_tau        = newmet_tst_tau->met() * 0.001;
    reco_met_jet        = newmet_tst_jet->met() * 0.001;
    reco_met_muons      = newmet_tst_muons->met() * 0.001;
    reco_met_softtrk    = newmet_tst_softtrk->met() * 0.001;

*/






    tree_->Fill();
    return StatusCode::SUCCESS;
}





void AthMETTriggerUpgradeTool::calcL1MET(int Ex, int Ey, float& MET, bool& Overflow)
{
    int METQ;
    calcL1METQ(Ex, Ey, METQ, Overflow);
    MET = sqrt(static_cast<float>(METQ));
}



void AthMETTriggerUpgradeTool::calcL1METQ(int Ex, int Ey, int& METQ, bool& Overflow)
{

    /** Greater of 2 values determines range of bits used */
    unsigned int absEx = abs(Ex);
    unsigned int absEy = abs(Ey);
    int max = (absEx > absEy ? absEx : absEy);

    /** If Ex/Ey overflows the LUT input range, trigger fires all thresholds.
        Indicate this with overflow flag and an out of range ETmiss value */
    if ( max >= (1<<(m_nBits+m_nRanges-1)) )  {
        METQ = 16777216; // 4096**2
        Overflow = true;
    }
        /** Otherwise, emulate precision by checking which ET range we are in and
            zeroing bits below that. */
    else {
        for (unsigned int range = 0; range < m_nRanges; ++range) {
            if ( max < (1<<(m_nBits+range)) ) {
                absEx &= (m_mask<<range);
                absEy &= (m_mask<<range);
                break;
            }
        }
        METQ = absEx*absEx + absEy*absEy;
        Overflow = false;
    }

}
