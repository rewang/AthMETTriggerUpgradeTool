#ifndef GENPLOTTER_GENPLOTTER_H
#define GENPLOTTER_GENPLOTTER_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!

#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"

#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TCanvas.h"
#include "TMath.h"

#include <string>
#include <map>
#include <vector>
#include "TString.h"
//
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SUSYTools/SUSYCrossSection.h"


// include files for using the trigger tools
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#define MAXPARTICLES 20

namespace Trig {
    class TrigDecisionTool;
    class MatchingTool;
}

class AthMETTriggerUpgradeTool: public ::AthAlgorithm {
public:
    AthMETTriggerUpgradeTool( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~AthMETTriggerUpgradeTool();

    virtual StatusCode  initialize();
    virtual StatusCode  execute();
    virtual StatusCode  finalize();

    void calcL1MET(int Ex, int Ey, float& MET, bool& Overflow);
    void calcL1METQ(int Ex, int Ey, int& METQ, bool& Overflow);

    int m_eventCounter; //!

private:

    static const unsigned int m_nBits = 6;
    static const unsigned int m_nRanges = 4;
    static const unsigned int m_mask = 0x3F;

    std::string jetType = "AntiKt4EMTopoJets";

    ToolHandle<Trig::TrigDecisionTool> m_trigDecTool;

//    ST::SUSYObjDef_xAOD *objTool; //!



    TTree* tree_;//!
    std::string m_sampleName; //!

    int event_num; //!
    int run_num; //!
    int npv, actMU, avgMU; //!


    bool  HLT_xe110_pufit_L1XE60;//!
    bool HLT_xe120_pufit_L1XE60 ;//!
    bool HLT_xe110_pufit_wEFMu_L1XE60;//!
    bool HLT_xe120_mht_xe80_L1XE60 ;//!
    bool HLT_xe120_trkmht_xe80_mht_L1XE60 ;//!
    bool  L1_KF_XE60; //!

    bool passL1XE50; //!
    float L1XE; //!
    float HLT_mht_metx, HLT_mht_mety; //!
    float HLT_cell_metx, HLT_cell_mety; //!
    float HLT_topocl_metx, HLT_topocl_mety; //!
    float HLT_pufit_metx, HLT_pufit_mety; //!
    float HLT_pueta_metx, HLT_pueta_mety; //!
    float HLT_trkmht_metx, HLT_trkmht_mety; //!


    float genmet_px, genmet_py;

    float raw_met, raw_met_ele, raw_met_gamma, raw_met_tau, raw_met_jet, raw_met_muons, raw_met_softtrk; //!
    float reco_met, reco_metphi, reco_met_ele, reco_met_gamma, reco_met_tau, reco_met_jet, reco_met_muons, reco_met_softtrk; //!

};

#endif //> !GENPLOTTER_GENPLOTTER_H
