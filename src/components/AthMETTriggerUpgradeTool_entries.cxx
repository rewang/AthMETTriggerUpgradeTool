
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../AthMETTriggerUpgradeTool.h"

DECLARE_ALGORITHM_FACTORY( AthMETTriggerUpgradeTool )

DECLARE_FACTORY_ENTRIES( AthMETTriggerUpgradeTool ) 
{
  DECLARE_ALGORITHM( AthMETTriggerUpgradeTool );
}
