//
//
#include "TEfficiency.h"
#include "/afs/cern.ch/user/r/rewang/Utils.h"

void logo(TString inputname, double yshift=0, double xshift=0)
{

    addText(0.57+xshift,0.87+xshift, 0.85+yshift,0.91+yshift , "#bf{#it{ATLAS}} Internal",kBlack);
    addText(0.57+xshift,0.87+xshift, 0.80+yshift,0.86+yshift , "MC16T, #sqrt{s} = 13 TeV",kBlack);
    //addText(0.57+xshift,0.87+xshift, 0.80+yshift,0.86+yshift , "Run 304008, #sqrt{s} = 13 TeV",kBlack);


    if(inputname.Contains("Zee")) addText(0.57+xshift,0.87+xshift, 0.75+yshift,0.81+yshift, "Z #rightarrow ee candidate events",kBlack);
    if(inputname.Contains("Wenu")) addText(0.57+xshift,0.87+xshift, 0.75+yshift,0.81+yshift, "W #rightarrow e#nu candidate events",kBlack);
    if(inputname.Contains("w1jet")) addText(0.57+xshift,0.87+xshift, 0.75+yshift,0.81+yshift, "W(#rightarrow e#nu) + 1jet candidate events",kBlack);
    if(inputname.Contains("w2jet")) addText(0.57+xshift,0.87+xshift, 0.75+yshift,0.81+yshift, "W(#rightarrow e#nu) + 2jets candidate events",kBlack);
    if(inputname.Contains("w3jet")) addText(0.57+xshift,0.87+xshift, 0.75+yshift,0.81+yshift, "W(#rightarrow e#nu) + 3jets candidate events",kBlack);
    if(inputname.Contains("w4jet")) addText(0.57+xshift,0.87+xshift, 0.75+yshift,0.81+yshift, "W(#rightarrow e#nu) + 4jets candidate events",kBlack);
    //addText(0.57+xshift,0.87+xshift, 0.75+yshift,0.81+yshift, "W(#rightarrow e#nu) + jets candidate events",kBlack);
    //addText(0.57+xshift,0.87+xshift, 0.75+yshift,0.81+yshift, "Z(#rightarrow #mu#mu) + jets candidate events",kBlack);


}

Double_t turnon_func(Double_t *x, Double_t *par)
{
    double halfpoint = par[0];
    double slope = par[1];
    double plateau = par[2];

    //double offset = par[3];
    //double plateau = 1.0;
    double offset = 0;

    double pt = TMath::Max(x[0],0.000001);

    double arg = 0;
    //cout << pt <<", "<< halfpoint <<", " << slope <<endl;
    arg = (pt - halfpoint)/(TMath::Sqrt(pt)*slope);
    double fitval = offset+0.5*plateau*(1+TMath::Erf(arg));
    return fitval;
}

Double_t turnon_func2(Double_t *x, Double_t *par)
{
    return turnon_func(x,par) + turnon_func(x,&par[3]);
}


void plotterEff_Single(TString InputName, TString histname="effzmmjetsvsMET_mht")
{

    TFile *infile = TFile::Open(InputName, "READ");

    vector<TString> All1DEffHists;
    vector<TString> All1DRateHists;
    vector<TString> All2DResoHists;
    vector<TString> All1DHists;
    vector<TString> All2DHists;


    vector<Color_t> LineColors;
    LineColors.push_back(kGray+2);
    LineColors.push_back(kGray+2);
    LineColors.push_back(kGray+2);
    LineColors.push_back(kGreen-2);
    LineColors.push_back(kGreen-2);
    LineColors.push_back(kGreen-2);
    LineColors.push_back(2);
    LineColors.push_back(kBlue-3);
    LineColors.push_back(kOrange+7);
    LineColors.push_back(6);
    LineColors.push_back(kAzure+2);
    LineColors.push_back(kRed+2);
    LineColors.push_back(kOrange-3);
    LineColors.push_back(kPink+5);
    LineColors.push_back(kGreen+4);

    vector<Color_t> MarkerColors;
    MarkerColors.push_back(kGray+2);
    MarkerColors.push_back(kGray+2);
    MarkerColors.push_back(kGray+2);
    MarkerColors.push_back(kGreen-2);
    MarkerColors.push_back(kGreen-2);
    MarkerColors.push_back(kGreen-2);





    MarkerColors.push_back(2);
    MarkerColors.push_back(kBlue-3);
    MarkerColors.push_back(kOrange+7);
    MarkerColors.push_back(6);
    MarkerColors.push_back(kAzure+2);
    MarkerColors.push_back(kRed+2);
    MarkerColors.push_back(kOrange-3);
    MarkerColors.push_back(kPink+5);
    MarkerColors.push_back(kGreen+4);

    vector<Style_t> MarkerStyles;
    MarkerStyles.push_back(21);
    MarkerStyles.push_back(24);
    MarkerStyles.push_back(25);
    MarkerStyles.push_back(21);
    MarkerStyles.push_back(24);
    MarkerStyles.push_back(25);

    vector<TString> Legends;
    Legends.push_back("HLT_mht110_L1XE50");




    //######################
    // efficiency plot
    //######################

    All1DEffHists.push_back("HLT_xe110_pufit_L1XE60");
    All1DEffHists.push_back("HLT_xe120_pufit_L1XE60");
    All1DEffHists.push_back("HLT_xe110_pufit_wEFMu_L1XE60");
    All1DEffHists.push_back("HLT_xe120_mht_xe80_L1XE60");
    All1DEffHists.push_back("HLT_xe120_trkmht_xe80_mht_L1XE60");
    All1DEffHists.push_back("L1_KF_XE60");







/*
    All1DHists.push_back("zpt");
    All1DHists.push_back("mht");
    All1DHists.push_back("cell");
    All1DHists.push_back("pufit");
    All1DHists.push_back("l1xe");
*/


























    //Set the plot style
    gStyle->SetOptStat(0);
    //gStyle->SetOptStat("emruoi");

    //Set up the canvas
    TCanvas *c = new TCanvas("c", "c", 700, 550);
    //TCanvas *c = new TCanvas("c", "c", 900, 550);
    TPad* t1 = new TPad("t1","t1", 0.0, 0.0, 1.0, 1.00);
    //t1->SetGrid();
    t1->Draw();
    t1->cd();
    //t1->SetBottomMargin(0.3);
    t1->SetRightMargin(0.03);
    //t1->SetLogx(1);
    //c->Divide(1,2);


    TLegend* legA  = new TLegend(0.45,0.22,0.96,0.40);//,"NDC");



    //Loop over the Fitting histograms
    for (size_t ihist = 0; ihist < All1DEffHists.size(); ihist++) {
        gStyle->SetOptFit(0);

        //Read in the histogram
        TString tag_i = All1DEffHists[ihist];

        TEfficiency* hist_i = (TEfficiency*) infile->Get(tag_i);

        if(ihist==0) hist_i->Draw("AP");
        else hist_i->Draw("P sames");







        hist_i->SetLineColor(LineColors[ihist]);
        hist_i->SetMarkerColor(MarkerColors[ihist]);
        hist_i->SetMarkerStyle(MarkerStyles[ihist]);



        t1->Update();
        hist_i->GetPaintedGraph()->GetYaxis()->SetTitle("Overlap rate");
        hist_i->GetPaintedGraph()->GetYaxis()->SetRangeUser(0,1.1);


        t1->Update();
        TString logo_histname = All1DEffHists[ihist];
	logo_histname.ReplaceAll("effzmmjets_","");
        if(ihist==0) logo(logo_histname,-0.1);
        legA->AddEntry(hist_i,logo_histname,"EPL");



    }

    legA->SetFillColor(0);
    legA->SetFillStyle(0);
    legA->SetLineColor(0);
    //legA->SetHeader("");
    legA->Draw("same");
    legA->SetTextFont(42);

    TLine *ll;
    ll = new TLine(1, 1, 440, 1);
    ll->SetLineWidth(2);
    ll->SetLineColor(kGray+2);
    ll->SetLineStyle(3);
    ll->Draw();



    //Save plots
    TString SaveName = InputName;
    SaveName.ReplaceAll(".root","");
    c->SaveAs("eff_"+ histname +".png");
    c->SaveAs("eff_"+ histname +".pdf");













    delete c;



    //Set the plot style
    //gStyle->SetOptStat(0);
    gStyle->SetOptStat("emruoi");


    //Loop over the 1D histograms
    for (size_t ihist = 0; ihist < All1DHists.size(); ihist++) {
        //Read in the histogram
        TH1F* hist = (TH1F*) infile->Get(All1DHists[ihist]);

        //Check if histogram actually exists
        if (hist == NULL) {
            cout << "hist is NULL!" << endl;
            continue;
        }

        //Set up the canvas
        TCanvas *c = new TCanvas("c", "c", 700, 550);
        //TCanvas *c = new TCanvas("c", "c", 900, 550);
        TPad* t1 = new TPad("t1","t1", 0.0, 0.0, 1.0, 1.00);
        t1->Draw();
        t1->cd();
        //t1->SetBottomMargin(0.3);
        t1->SetRightMargin(0.03);
        t1->SetLogy(1);
        //c->Divide(1,2);

        //Draw the histogram on the canvas
        hist->Draw("hist");

        //Make plot look nice
        //hx_->GetXaxis()->SetTitle("Vertices");
        //hist->GetYaxis()->SetTitle("Events");
        hist->SetFillColor(kYellow);
        if(All1DHists[ihist].Contains("Zee")) hist->SetFillColor(kBlue-7);

        logo(All1DHists[ihist]);


        //Save plots
        c->SaveAs(All1DHists[ihist]+".png");
        c->SaveAs(All1DHists[ihist]+".pdf");

        //delete pointers
        delete c;
        delete hist;
    }



    infile->Close();


}



