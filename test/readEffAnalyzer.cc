
#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

#include <string.h>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <set>
#include <cmath>

#include "Math/LorentzVector.h"
#include "TMath.h"
#include "TVector2.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include "TEfficiency.h"
#include "TH2F.h"

#include "Utils.h"


#define MAXPARTICLES 9999

double deltaPhi(double phi1, double phi2)
{
    double result = phi1 - phi2;
    while (result > M_PI) result -= 2*M_PI;
    while (result <= -M_PI) result += 2*M_PI;
    return result;
}

double deltaR2(double eta1, double phi1, double eta2, double phi2)
{
    double deta = eta1 - eta2;
    double dphi = deltaPhi(phi1, phi2);
    return deta*deta + dphi*dphi;
}

double deltaR(double eta1, double phi1, double eta2, double phi2)
{
    return std::sqrt(deltaR2 (eta1, phi1, eta2, phi2));
}


float transverseMass(TLorentzVector h1, TLorentzVector h2)
{
    double dphi= h1.DeltaPhi(h2);
    return TMath::Sqrt(2*h1.Pt()*h2.Pt()*(1-TMath::Cos(dphi)));

}



void readEffAnalyzer(TString _input_="./xxx.root", TString _output_="output_xxx.root")
{

    TFile *InFile = TFile::Open(_input_, "READ");

    // ########################################
    // retrive information from tree
    // ########################################
    TTree *t_ = (TTree *)InFile->Get("data");
    int event_num; //!
    int run_num; //!
    int npv, actMU, avgMU; //!
    bool passL1XE50; //!
    float PVz; //!

    //default
    float L1XE; //!
    float HLT_mht_metx, HLT_mht_mety; //!
    float HLT_cell_metx, HLT_cell_mety; //!
    float HLT_topocl_metx, HLT_topocl_mety; //!
    float HLT_pufit_metx, HLT_pufit_mety; //!
    float HLT_pueta_metx, HLT_pueta_mety; //!


    bool  HLT_xe110_pufit_L1XE60;//!
    bool HLT_xe120_pufit_L1XE60 ;//!
    bool HLT_xe110_pufit_wEFMu_L1XE60;//!
    bool HLT_xe120_mht_xe80_L1XE60 ;//!
    bool HLT_xe120_trkmht_xe80_mht_L1XE60 ;//!
    bool  L1_KF_XE60; //!


    float raw_met, raw_met_ele, raw_met_gamma, raw_met_tau, raw_met_jet, raw_met_muons, raw_met_softtrk; //!
    float reco_met, reco_metphi, reco_met_ele, reco_met_gamma, reco_met_tau, reco_met_jet, reco_met_muons, reco_met_softtrk; //!


    float genmet_px, genmet_py;//!

    t_->SetBranchAddress("event_num", &event_num);
    t_->SetBranchAddress("run_num", &run_num);
    t_->SetBranchAddress("npv", &npv);
    t_->SetBranchAddress("actMU", &actMU);
    t_->SetBranchAddress("avgMU", &avgMU);
    t_->SetBranchAddress("PVz", &PVz);

    //default
    t_->SetBranchAddress("passL1XE50", &passL1XE50);
    t_->SetBranchAddress("L1XE", &L1XE);

    t_->SetBranchAddress("HLT_mht_metx", &HLT_mht_metx);
    t_->SetBranchAddress("HLT_mht_mety", &HLT_mht_mety);
    t_->SetBranchAddress("HLT_cell_metx", &HLT_cell_metx);
    t_->SetBranchAddress("HLT_cell_mety", &HLT_cell_mety);
    t_->SetBranchAddress("HLT_topocl_metx", &HLT_topocl_metx);
    t_->SetBranchAddress("HLT_topocl_mety", &HLT_topocl_mety);
    t_->SetBranchAddress("HLT_pufit_metx", &HLT_pufit_metx);
    t_->SetBranchAddress("HLT_pufit_mety", &HLT_pufit_mety);
    t_->SetBranchAddress("HLT_pueta_metx", &HLT_pueta_metx);
    t_->SetBranchAddress("HLT_pueta_mety", &HLT_pueta_mety);


    t_->SetBranchAddress("genmet_px",    &genmet_px);
    t_->SetBranchAddress("genmet_py",    &genmet_py);




    t_->SetBranchAddress("HLT_xe110_pufit_L1XE60", &HLT_xe110_pufit_L1XE60);
    t_->SetBranchAddress("HLT_xe120_pufit_L1XE60", &HLT_xe120_pufit_L1XE60);
    t_->SetBranchAddress("HLT_xe110_pufit_wEFMu_L1XE60", &HLT_xe110_pufit_wEFMu_L1XE60);
    t_->SetBranchAddress("HLT_xe120_mht_xe80_L1XE60", &HLT_xe120_mht_xe80_L1XE60);
    t_->SetBranchAddress("HLT_xe120_trkmht_xe80_mht_L1XE60", &HLT_xe120_trkmht_xe80_mht_L1XE60);
    t_->SetBranchAddress("L1_KF_XE60", &L1_KF_XE60);


    t_->SetBranchAddress("raw_met_ele", &raw_met_ele);
    t_->SetBranchAddress("raw_met_gamma", &raw_met_gamma);
    t_->SetBranchAddress("raw_met_tau", &raw_met_tau);
    t_->SetBranchAddress("raw_met_jet", &raw_met_jet);
    t_->SetBranchAddress("raw_met_muons", &raw_met_muons);
    t_->SetBranchAddress("raw_met_softtrk", &raw_met_softtrk);
    t_->SetBranchAddress("raw_met", &raw_met);

    t_->SetBranchAddress("reco_met_ele", &reco_met_ele);
    t_->SetBranchAddress("reco_met_gamma", &reco_met_gamma);
    t_->SetBranchAddress("reco_met_tau", &reco_met_tau);
    t_->SetBranchAddress("reco_met_jet", &reco_met_jet);
    t_->SetBranchAddress("reco_met_muons", &reco_met_muons);
    t_->SetBranchAddress("reco_met_softtrk", &reco_met_softtrk);
    t_->SetBranchAddress("reco_met", &reco_met);
    t_->SetBranchAddress("reco_metphi", &reco_metphi);




    // ########################################
    // initialize histograms
    // ########################################

    double METEffBins[]= {0,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,210,220,230,240,250,300,350,400};
    const int nMETEffBins = sizeof(METEffBins)/sizeof(double) - 1;


    double PVBins[]= {0,15,18,20,22,24,26,28,30,32,34,36,40};
    const int nPVBins = sizeof(PVBins)/sizeof(double) - 1;




    TEfficiency *h_HLT_xe110_pufit_L1XE60 = new TEfficiency("HLT_xe110_pufit_L1XE60","; Truth E^{miss}_{T} [GeV]; Efficiency ",50,0,400);
    TEfficiency *h_HLT_xe120_pufit_L1XE60 = new TEfficiency("HLT_xe120_pufit_L1XE60","; Truth E^{miss}_{T} [GeV]; Efficiency ",50,0,400);
    TEfficiency *h_HLT_xe110_pufit_wEFMu_L1XE60 = new TEfficiency("HLT_xe110_pufit_wEFMu_L1XE60","; Truth E^{miss}_{T} [GeV]; Efficiency ",50,0,400);

    TEfficiency *h_HLT_xe120_mht_xe80_L1XE60 = new TEfficiency("HLT_xe120_mht_xe80_L1XE60","; Truth E^{miss}_{T} [GeV]; Efficiency ",50,0,400);
    TEfficiency *h_HLT_xe120_trkmht_xe80_mht_L1XE60 = new TEfficiency("HLT_xe120_trkmht_xe80_mht_L1XE60","; Truth E^{miss}_{T} [GeV]; Efficiency ",50,0,400);
    TEfficiency *h_L1_KF_XE60 = new TEfficiency("L1_KF_XE60","; Truth E^{miss}_{T} [GeV]; Efficiency ",50,0,400);







    // ########################################
    // Loop events
    // ########################################
    int evStart = 0;
    int evEnd = -1;
    printf("Progressing Bar     :0%%       20%%       40%%       60%%       80%%       100%%\n");
    printf("Scanning the ntuple :");
    // all entries and fill the histograms
    Int_t nentries = (Int_t)t_->GetEntries();
    int treeStep = nentries/50;

    for (Int_t i=0; i<nentries; i++) {
        t_->GetEntry(i);

        //if(i>50) break;

        if((i-evStart)%treeStep==0) {
            printf("#");
            fflush(stdout);
        }


        float genmet = sqrt(genmet_px*genmet_px + genmet_py*genmet_py);

        h_HLT_xe110_pufit_L1XE60->Fill(HLT_xe110_pufit_L1XE60,genmet);
        h_HLT_xe120_pufit_L1XE60->Fill(HLT_xe120_pufit_L1XE60,genmet);
        h_HLT_xe110_pufit_wEFMu_L1XE60->Fill(HLT_xe110_pufit_wEFMu_L1XE60,genmet);
        h_HLT_xe120_mht_xe80_L1XE60->Fill(HLT_xe120_mht_xe80_L1XE60,genmet);
        h_HLT_xe120_trkmht_xe80_mht_L1XE60->Fill(HLT_xe120_trkmht_xe80_mht_L1XE60,genmet);
        h_L1_KF_XE60->Fill(L1_KF_XE60, genmet);


        //return;
    }

    cout <<"\n";



    // ########################################
    // saving outputs
    // ########################################
    TFile *OutFile = TFile::Open(_output_, "RECREATE");



    h_HLT_xe110_pufit_L1XE60->Write();
    h_HLT_xe120_pufit_L1XE60->Write();
    h_HLT_xe110_pufit_wEFMu_L1XE60->Write();
    h_HLT_xe120_mht_xe80_L1XE60->Write();
    h_HLT_xe120_trkmht_xe80_mht_L1XE60->Write();

    h_L1_KF_XE60->Write();




    OutFile->Close();

    InFile->Close();
}

